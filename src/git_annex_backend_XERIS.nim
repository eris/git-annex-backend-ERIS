# SPDX-FileCopyrightText: 2022 ☭ Endo Renberg
# SPDX-License-Identifier: Unlicense

import std/[asyncdispatch, monotimes, os, parseutils, streams, times]
import base32
import eris

const backendPrefix = "XERIS"

proc respond(words: varargs[string, `$`]) =
  for i, word in words:
    if i > 0: stdout.write " "
    stdout.write word
  stdout.write('\n')
  flushFile stdout

proc exit(msg: string) =
  respond("ERROR", msg)
  quit QuitFailure

proc toAnnexKey(cap: ErisCap; size: BiggestInt): string =
  backendPrefix & "-s" & $size & "--" & cap.toBase32

proc toErisCap(key: string): ErisCap =
  const b32Len = 105
  var token: string
  discard parseUntil(key, token, '-')
  if token != backendPrefix:
    exit "invalid key prefix"
  elif key.len < b32Len:
    exit "invalid key length"
  else:
    let bin = base32.decode key[(key.high - b32Len)..key.high]
    result = parseCap(bin)

type CustomStore = ref object of ErisStoreObj
  progress: BiggestInt
  deadline: MonoTime

method put(store: CustomStore; f: FuturePut) =
  store.progress.inc f.chunkSize.int
  let now = getMonoTime()
  if store.deadline < now:
    store.deadline = now + initDuration(seconds = 1)
    respond "PROGRESS", store.progress
  f.complete()

proc newCustomStore*(): CustomStore = new result
  ## Create an ``ErisStore`` that discards writes and fails to read.

proc genCap(path: string; bs: ChunkSize): ErisCap =
  var stream = openFileStream(path, bufSize = chunk32k.int)
  var n: uint64
  (result, n) = waitFor(encode(newCustomStore(), bs, stream, convergentMode))
  close stream

proc main() =
  var line, command: string
  while stdin.readLine(line):
    var off = parseUntil(line, command, ' ').succ
    proc nextArg(): string =
      off.inc parseUntil(line, result, ' ', off).succ
    proc finalArg(): string =
      result = line[off..line.high]
    try:
      case command
      of "GETVERSION":
        respond "VERSION 1"
      of "CANVERIFY":
        respond "CANVERIFY-YES"
      of "ISSTABLE":
        respond "ISSTABLE-YES"
      of "ISCRYPTOGRAPHICALLYSECURE":
        respond "ISCRYPTOGRAPHICALLYSECURE-YES"
      of "GENKEY":
        let
          path = finalArg()
          size = getFileSize path
          cap = genCap(path, recommendedChunkSize(size))
        respond "GENKEY-SUCCESS", toAnnexKey(cap, size)
      of "VERIFYKEYCONTENT":
        let
          key = nextArg()
          path = finalArg()
          want = key.toErisCap
          have = genCap(path, want.chunkSize)
        if want == have:
          respond "VERIFYKEYCONTENT-SUCCESS"
        else:
          respond "VERIFYKEYCONTENT-FAILURE"
      else:
        exit "unknown command"
    except CatchableError as e:
      let e = getCurrentException()
      respond "ERROR", e.msg
      quit QuitFailure

when isMainModule: main()
